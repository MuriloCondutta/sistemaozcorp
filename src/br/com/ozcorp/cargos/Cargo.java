/**
 * Este objeto � utilizado para criar instancias de cargos dos funcion�rios.
 * 
 * Todos os cargos possuem um t�tulo, um sal�rio base, n�vel de acesso e est�o em um departamento.
 * 
 * @author Murilo Afonso Condutta
 */

package br.com.ozcorp.cargos;

import br.com.ozcorp.departamentos.TiposDepartamentos;

public class Cargo {

	private TiposCargos titulo;
	private TiposCargos nivelAcesso;
	private double salario;
	private TiposDepartamentos departamento;
	
	
	//Getters and Setters
	public TiposCargos getTitulo() {
		return titulo;
	}
	public void setTitulo(TiposCargos titulo) {
		this.titulo = titulo;
	}
	public TiposCargos getNivelAcesso() {
		return nivelAcesso;
	}
	public void setNivelAcesso(TiposCargos nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public TiposDepartamentos getDepartamento() {
		return departamento;
	}
	public void setDepartamento(TiposDepartamentos departamento) {
		this.departamento = departamento;
	}

	//Construtores
	public Cargo(TiposCargos titulo, TiposCargos nivelAcesso, double salario, TiposDepartamentos departamento) {
		super();
		this.titulo = titulo;
		this.nivelAcesso = titulo;
		this.salario = salario;
		this.departamento = departamento;
	}
	
}
