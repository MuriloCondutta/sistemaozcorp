/**
 * Esta enumera��o diz os cargos que a empresa possui, bem como o n�vel de acesso dos tipos de funcion�rios.
 * 
 * @author Murilo Afonso Condutta
 */

package br.com.ozcorp.cargos;

public enum TiposCargos {

		Gerente("2"), Diretor("0"), Secret�rio("1"), Engenheiro("3"), Analista("4");
	
	public String nivelAcesso;
	
	TiposCargos(String nivelAcesso){
		
		this.nivelAcesso = nivelAcesso;
	}
}
