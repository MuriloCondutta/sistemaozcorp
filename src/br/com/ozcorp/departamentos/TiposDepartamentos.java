/**
 * Esta enumera��o fornece os departamentos existentes na empresa, com sua sigla e seu t�tulo.
 * 
 * @author Murilo Afonso Condutta
 */

package br.com.ozcorp.departamentos;

public enum TiposDepartamentos {
	
	DF("Financeiro"), 
	DV("de Vendas"), 
	DA("Administrativo"), 
	DM("de Marketing");
	
	public String t;
	
	TiposDepartamentos(String t) {
		
		this.t = t;
		
	}
	
}
