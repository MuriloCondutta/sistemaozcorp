package br.com.ozcorp.funcionario;

public enum TiposSangue {

		Ap("A+"), Bp("B+"), ABp("AB+"), Op("O+"), An("A-"), Bn("B-"), ABn("AB-"), On("O-");
	
	public String simbolo;
	
	TiposSangue(String simbolo){
		this.simbolo = simbolo;
	}
	
}
