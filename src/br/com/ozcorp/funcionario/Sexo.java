/**
 * Enumeração para limitar o sexo do funcionário.
 * 
 * @author Murilo Afonso Condutta
 */

package br.com.ozcorp.funcionario;

public enum Sexo {
	M("Masculino"), F("Feminino");

	public String s;

	Sexo(String s) {

		this.s = s;

	}
}
