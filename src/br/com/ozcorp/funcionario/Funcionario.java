/**
 * Esta classe � utilizada para criar funcion�rios no sistema da empresa.
 * 
 * @author Murilo Afonso Condutta
 */

package br.com.ozcorp.funcionario;

import br.com.ozcorp.cargos.Cargo;

public class Funcionario {

	// Atributos
	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String email;
	private Cargo cargo;
	private String senha;
	private TiposSangue tipoSangue;
	private Sexo sexo;

	// Getters and Setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TiposSangue getTipoSangue() {
		return tipoSangue;
	}

	public void setTipoSangue(TiposSangue tipoSangue) {
		this.tipoSangue = tipoSangue;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	// Construtores
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, Cargo cargo, String senha,
			TiposSangue tipoSangue, Sexo sexo) {
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.cargo = cargo;
		this.senha = senha;
		this.tipoSangue = tipoSangue;
		this.sexo = sexo;
		
	}

}
