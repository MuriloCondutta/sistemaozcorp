/**
 * Este programa realiza a administra��o dos departamentos, cargos e funcion�rios da empreza OzCorp.
 * 
 * Ele realiza isso atrav�s de outras classes, criando inst�ncias de objetos.
 * 
 * O objetivo � criar inst�ncias de cada tipo de funcion�rio com suas informa��es.
 * 
 * @author Murilo Afonso Condutta
 */

package br.com.ozcorp;

import br.com.ozcorp.cargos.Cargo;
import br.com.ozcorp.cargos.TiposCargos;
import br.com.ozcorp.departamentos.TiposDepartamentos;
import br.com.ozcorp.funcionario.Funcionario;
import br.com.ozcorp.funcionario.Sexo;
import br.com.ozcorp.funcionario.TiposSangue;

public class SistemaPrincipal {

	public static void main(String[] args) {
		
		//Guilerme - Gerente Administrativo
		
		Funcionario guilherme = new Funcionario("Guilherme", "56987412-05", "559236145-08", "Matricula448910",
				"guilhermefuncionario@hotmail.com", new Cargo(TiposCargos.Gerente, TiposCargos.Gerente, 10_000, TiposDepartamentos.DA),
				"1123!", TiposSangue.Ap, Sexo.M);

		// Impress�o dos dados do funcion�rio
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome: " + guilherme.getNome());
		System.out.println("RG : " + guilherme.getRg());
		System.out.println("CPF: " + guilherme.getCpf());
		System.out.println("Matr�cula: " + guilherme.getMatricula());
		System.out.println("E-mail: " + guilherme.getEmail());
		System.out.println("Senha: " + guilherme.getSenha());
		System.out.println("Cargo: " + guilherme.getCargo().getTitulo() + " " + guilherme.getCargo().getDepartamento().t);
		System.out.println("N�vel de Acesso: " + guilherme.getCargo().getNivelAcesso().nivelAcesso);
		System.out.println("Sal�rio-base: " + "R$ " + guilherme.getCargo().getSalario());
		System.out.println("Departamento: " + guilherme.getCargo().getDepartamento().t);
		System.out.println("Sexo: " + guilherme.getSexo().s);
		System.out.println("Tipos Sangu�neo: " + guilherme.getTipoSangue().simbolo);
		
		System.out.println("-------------------------------------------");
		//Ana Julia - Secret�rio(a) Financeiro
		
		Funcionario ana = new Funcionario("Ana Julia", "23256214-05", "546132896-56", "Matricula448911",
				"anafuncionario@hotmail.com", new Cargo(TiposCargos.Secret�rio, TiposCargos.Secret�rio, 10_000, TiposDepartamentos.DF),
				"11234!", TiposSangue.ABn, Sexo.F);

		// Impress�o dos dados do funcion�rio
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome: " + ana.getNome());
		System.out.println("RG : " + ana.getRg());
		System.out.println("CPF: " + ana.getCpf());
		System.out.println("Matr�cula: " + ana.getMatricula());
		System.out.println("E-mail: " + ana.getEmail());
		System.out.println("Senha: " + ana.getSenha());
		System.out.println("Cargo: " + ana.getCargo().getTitulo() + " " + ana.getCargo().getDepartamento().t);
		System.out.println("N�vel de Acesso: " + ana.getCargo().getNivelAcesso().nivelAcesso);
		System.out.println("Sal�rio-base: " + "R$ " + ana.getCargo().getSalario());
		System.out.println("Departamento: " + ana.getCargo().getDepartamento().t);
		System.out.println("Sexo: " + ana.getSexo().s);
		System.out.println("Tipos Sangu�neo: " + ana.getTipoSangue().simbolo);
		
		System.out.println("-------------------------------------------");
		//Luiza - Diretor(a)de Marketing
		
		Funcionario luiza = new Funcionario("Luiza", "23248214-06", "447147896-56", "Matricula448912",
				"luizafuncionario@hotmail.com", new Cargo(TiposCargos.Diretor, TiposCargos.Diretor, 20_000, TiposDepartamentos.DM),
				"1124!", TiposSangue.Bn, Sexo.F);

		// Impress�o dos dados do funcion�rio
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome: " + luiza.getNome());
		System.out.println("RG : " + luiza.getRg());
		System.out.println("CPF: " + luiza.getCpf());
		System.out.println("Matr�cula: " + luiza.getMatricula());
		System.out.println("E-mail: " + luiza.getEmail());
		System.out.println("Senha: " + luiza.getSenha());
		System.out.println("Cargo: " + luiza.getCargo().getTitulo() + " " + luiza.getCargo().getDepartamento().t);
		System.out.println("N�vel de Acesso: " + luiza.getCargo().getNivelAcesso().nivelAcesso);
		System.out.println("Sal�rio-base: " + "R$ " + luiza.getCargo().getSalario());
		System.out.println("Departamento: " + luiza.getCargo().getDepartamento().t);
		System.out.println("Sexo: " + luiza.getSexo().s);
		System.out.println("Tipos Sangu�neo: " + luiza.getTipoSangue().simbolo);
		
		System.out.println("-------------------------------------------");
		//Marcos - Engenheiro(a) Financeiro
		
		Funcionario marcos = new Funcionario("Marcos", "12345214-06", "447112346-56", "Matricula448913",
				"marcosfuncionario@hotmail.com", new Cargo(TiposCargos.Engenheiro, TiposCargos.Engenheiro, 15_000, TiposDepartamentos.DF),
				"112345!", TiposSangue.ABp, Sexo.M);

		// Impress�o dos dados do funcion�rio
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome: " + marcos.getNome());
		System.out.println("RG : " + marcos.getRg());
		System.out.println("CPF: " + marcos.getCpf());
		System.out.println("Matr�cula: " + marcos.getMatricula());
		System.out.println("E-mail: " + marcos.getEmail());
		System.out.println("Senha: " + marcos.getSenha());
		System.out.println("Cargo: " + marcos.getCargo().getTitulo() + " " + marcos.getCargo().getDepartamento().t);
		System.out.println("N�vel de Acesso: " + marcos.getCargo().getNivelAcesso().nivelAcesso);
		System.out.println("Sal�rio-base: " + "R$ " + marcos.getCargo().getSalario());
		System.out.println("Departamento: " + marcos.getCargo().getDepartamento().t);
		System.out.println("Sexo: " + marcos.getSexo().s);
		System.out.println("Tipos Sangu�neo: " + marcos.getTipoSangue().simbolo);
		
		System.out.println("-------------------------------------------");
		//Val�ria - Analista(a) de Vendas
		
		Funcionario valeria = new Funcionario("Val�ria", "12321224-14", "212352346-16", "Matricula448914",
				"valeiafuncionario@hotmail.com", new Cargo(TiposCargos.Analista, TiposCargos.Analista, 25_000, TiposDepartamentos.DV),
				"11234545!", TiposSangue.Op, Sexo.F);

		// Impress�o dos dados do funcion�rio
		System.out.println("DADOS DO FUNCION�RIO: ");
		System.out.println("Nome: " + valeria.getNome());
		System.out.println("RG : " + valeria.getRg());
		System.out.println("CPF: " + valeria.getCpf());
		System.out.println("Matr�cula: " + valeria.getMatricula());
		System.out.println("E-mail: " + valeria.getEmail());
		System.out.println("Senha: " + valeria.getSenha());
		System.out.println("Cargo: " + valeria.getCargo().getTitulo() + " " + valeria.getCargo().getDepartamento().t);
		System.out.println("N�vel de Acesso: " + valeria.getCargo().getNivelAcesso().nivelAcesso);
		System.out.println("Sal�rio-base: " + "R$ " + valeria.getCargo().getSalario());
		System.out.println("Departamento: " + valeria.getCargo().getDepartamento().t);
		System.out.println("Sexo: " + valeria.getSexo().s);
		System.out.println("Tipos Sangu�neo: " + valeria.getTipoSangue().simbolo);
		
	}

}
